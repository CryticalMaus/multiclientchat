// MessageLogSystem.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "MessageLogSystem.h"
#include <fstream>

// This is an example of an exported function.
EXPORT void LogMessage(const char *message)
{
	std::ofstream outfile("LogFile.txt", std::ofstream::app);

	outfile << std::string(message);
}
