﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VPNGui
{
    public partial class LoginWindow : Form
    {
        Backend UI;

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void check_box_showpassword_CheckedChanged(object sender, EventArgs e)
        {
            this.text_entry_password.UseSystemPasswordChar = !this.checkbox_showpassword.Checked;
        }

        private void LoginWindow_Load(object sender, EventArgs e)
        {
            this.UI = new Backend(this);
            this.UI.init();
        }

        private void button_login_Click(object sender, EventArgs e)
        {
            this.UI.LogIn(this.text_entry_username.Text, this.text_entry_password.Text, this.checkbox_rememberme.Checked);
            this.main_group.Visible = false;
            this.RuntimeBox.Visible = true;
        }

        private void ChatEntry_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.UI.SendMessage(this.ChatEntry.Text);
                this.ChatEntry.Text = "";

                e.Handled = true;
            }
        }

        private void ChatEntry_EnterConsume(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
            }
        }

        public delegate void UpdateChatDisplayDelegate(string message);
        public void AddMessage(string message)
        {
            // InvokeRequired required compares the thread ID of the  
            // calling thread to the thread ID of the creating thread.  
            // If these threads are different, it returns true.  
            if (this.ChatDisplay.InvokeRequired)
            {
                UpdateChatDisplayDelegate d = new UpdateChatDisplayDelegate(AddMessage);
                this.Invoke(d, new object[] { message });
            }
            else
            {
                this.ChatDisplay.Text += message;
            }
        }
    }
}
