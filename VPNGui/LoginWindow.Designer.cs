﻿namespace VPNGui
{
    partial class LoginWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginWindow));
            this.text_entry_username = new System.Windows.Forms.TextBox();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.text_entry_password = new System.Windows.Forms.TextBox();
            this.label_username = new System.Windows.Forms.Label();
            this.label_password = new System.Windows.Forms.Label();
            this.checkbox_showpassword = new System.Windows.Forms.CheckBox();
            this.main_group = new System.Windows.Forms.GroupBox();
            this.checkbox_rememberme = new System.Windows.Forms.CheckBox();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.button_login = new System.Windows.Forms.Button();
            this.RuntimeBox = new System.Windows.Forms.GroupBox();
            this.ChatDisplay = new System.Windows.Forms.RichTextBox();
            this.ChatEntry = new System.Windows.Forms.TextBox();
            this.main_group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.RuntimeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // text_entry_username
            // 
            this.text_entry_username.Location = new System.Drawing.Point(71, 143);
            this.text_entry_username.MaxLength = 256;
            this.text_entry_username.Name = "text_entry_username";
            this.text_entry_username.Size = new System.Drawing.Size(183, 20);
            this.text_entry_username.TabIndex = 0;
            this.text_entry_username.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChatEntry_EnterConsume);
            // 
            // text_entry_password
            // 
            this.text_entry_password.Location = new System.Drawing.Point(71, 169);
            this.text_entry_password.MaxLength = 256;
            this.text_entry_password.Name = "text_entry_password";
            this.text_entry_password.Size = new System.Drawing.Size(183, 20);
            this.text_entry_password.TabIndex = 1;
            this.text_entry_password.UseSystemPasswordChar = true;
            this.text_entry_password.Text = "ops tech";
            this.text_entry_password.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChatEntry_EnterConsume);
            // 
            // label_username
            // 
            this.label_username.AutoSize = true;
            this.label_username.Location = new System.Drawing.Point(12, 146);
            this.label_username.Name = "label_username";
            this.label_username.Size = new System.Drawing.Size(53, 13);
            this.label_username.TabIndex = 2;
            this.label_username.Text = "username";
            // 
            // label_password
            // 
            this.label_password.AutoSize = true;
            this.label_password.Location = new System.Drawing.Point(12, 169);
            this.label_password.Name = "label_password";
            this.label_password.Size = new System.Drawing.Size(52, 13);
            this.label_password.TabIndex = 4;
            this.label_password.Text = "password";
            // 
            // checkbox_showpassword
            // 
            this.checkbox_showpassword.AutoSize = true;
            this.checkbox_showpassword.Location = new System.Drawing.Point(155, 195);
            this.checkbox_showpassword.Name = "checkbox_showpassword";
            this.checkbox_showpassword.Size = new System.Drawing.Size(99, 17);
            this.checkbox_showpassword.TabIndex = 5;
            this.checkbox_showpassword.Text = "show password";
            this.checkbox_showpassword.UseVisualStyleBackColor = true;
            this.checkbox_showpassword.CheckedChanged += new System.EventHandler(this.check_box_showpassword_CheckedChanged);
            // 
            // main_group
            // 
            this.main_group.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.main_group.BackColor = System.Drawing.Color.Transparent;
            this.main_group.Controls.Add(this.checkbox_rememberme);
            this.main_group.Controls.Add(this.Logo);
            this.main_group.Controls.Add(this.checkbox_showpassword);
            this.main_group.Controls.Add(this.label_password);
            this.main_group.Controls.Add(this.label_username);
            this.main_group.Controls.Add(this.text_entry_password);
            this.main_group.Controls.Add(this.text_entry_username);
            this.main_group.Location = new System.Drawing.Point(12, 112);
            this.main_group.Name = "main_group";
            this.main_group.Size = new System.Drawing.Size(260, 237);
            this.main_group.TabIndex = 7;
            this.main_group.TabStop = false;
            // 
            // checkbox_rememberme
            // 
            this.checkbox_rememberme.AutoSize = true;
            this.checkbox_rememberme.Location = new System.Drawing.Point(155, 214);
            this.checkbox_rememberme.Name = "checkbox_rememberme";
            this.checkbox_rememberme.Size = new System.Drawing.Size(89, 17);
            this.checkbox_rememberme.TabIndex = 7;
            this.checkbox_rememberme.Text = "remember me";
            this.checkbox_rememberme.UseVisualStyleBackColor = true;
            // 
            // Logo
            // 
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(30, 20);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(200, 100);
            this.Logo.TabIndex = 6;
            this.Logo.TabStop = false;
            // 
            // button_login
            // 
            this.button_login.BackColor = System.Drawing.Color.White;
            this.button_login.Location = new System.Drawing.Point(83, 355);
            this.button_login.Name = "button_login";
            this.button_login.Size = new System.Drawing.Size(112, 36);
            this.button_login.TabIndex = 8;
            this.button_login.Text = "Log In";
            this.button_login.UseVisualStyleBackColor = false;
            this.button_login.Click += new System.EventHandler(this.button_login_Click);
            // 
            // RuntimeBox
            // 
            this.RuntimeBox.BackColor = System.Drawing.Color.Transparent;
            this.RuntimeBox.Controls.Add(this.ChatDisplay);
            this.RuntimeBox.Controls.Add(this.ChatEntry);
            this.RuntimeBox.Location = new System.Drawing.Point(12, 12);
            this.RuntimeBox.Name = "RuntimeBox";
            this.RuntimeBox.Size = new System.Drawing.Size(260, 437);
            this.RuntimeBox.TabIndex = 9;
            this.RuntimeBox.TabStop = false;
            this.RuntimeBox.Visible = false;
            // 
            // ChatDisplay
            // 
            this.ChatDisplay.Location = new System.Drawing.Point(0, 0);
            this.ChatDisplay.Name = "ChatDisplay";
            this.ChatDisplay.ReadOnly = true;
            this.ChatDisplay.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.ChatDisplay.Size = new System.Drawing.Size(260, 404);
            this.ChatDisplay.TabIndex = 2;
            this.ChatDisplay.Text = "";
            // 
            // ChatEntry
            // 
            this.ChatEntry.AcceptsTab = true;
            this.ChatEntry.Location = new System.Drawing.Point(6, 411);
            this.ChatEntry.MaxLength = 512;
            this.ChatEntry.Name = "ChatEntry";
            this.ChatEntry.Size = new System.Drawing.Size(248, 20);
            this.ChatEntry.TabIndex = 0;
            this.ChatEntry.TabStop = false;
            this.ChatEntry.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ChatEntry_Enter);
            this.ChatEntry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChatEntry_EnterConsume);
            // 
            // LoginWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(284, 461);
            this.Controls.Add(this.RuntimeBox);
            this.Controls.Add(this.button_login);
            this.Controls.Add(this.main_group);
            this.MinimumSize = new System.Drawing.Size(300, 500);
            this.Name = "LoginWindow";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.LoginWindow_Load);
            this.main_group.ResumeLayout(false);
            this.main_group.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.RuntimeBox.ResumeLayout(false);
            this.RuntimeBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox text_entry_username;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.TextBox text_entry_password;
        private System.Windows.Forms.Label label_username;
        private System.Windows.Forms.Label label_password;
        private System.Windows.Forms.CheckBox checkbox_showpassword;
        private System.Windows.Forms.GroupBox main_group;
        private System.Windows.Forms.CheckBox checkbox_rememberme;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.Button button_login;
        private System.Windows.Forms.GroupBox RuntimeBox;
        private System.Windows.Forms.RichTextBox ChatDisplay;
        private System.Windows.Forms.TextBox ChatEntry;
    }
}

