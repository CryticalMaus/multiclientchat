﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace VPNGui
{
    class Backend
    {
        static string PubKeyFile = @".\rsaPublicKey.txt";
        static string KeyName = "KeyPair";

        //encryption
        CspParameters CSP_Parameters = new CspParameters();
        RSACryptoServiceProvider rsa;

        //connections
        byte[] buffer = new byte[1024];
        Socket socket;
        String Username;
        Thread Listener;
        bool Run = true;

        //Gui connection
        LoginWindow Window;

        private int ReceiveFullMessage()
        {
            int byte_count = 0;
            int message_length = -1;
            while (true)
            {
                try
                {
                    byte_count += this.socket.Receive(this.buffer);

                    if (byte_count < 4)
                    {
                        continue;
                    }
                    if (byte_count > this.buffer.Length)
                    {
                        return -1;
                    }
                    if (message_length < 0)
                    {
                        message_length = BitConverter.ToInt32(this.buffer, 0);
                    }
                    if (byte_count < message_length)
                    {
                        continue;
                    }

                    return message_length;
                }
                catch (TimeoutException)
                {
                    return -2;
                }
                catch (System.Net.Sockets.SocketException)
                {
                    return -3;
                }
            }
        }

        private bool WriteByteArray(string fileName, byte[] byteArray)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                fs.Write(byteArray, 0, byteArray.Length);
                return true;
            }
        }

        private byte[] ReadByteArray(string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                byte[] array = new byte[fs.Length];
                fs.Read(array, 0, array.Length);
                return array;
            }
        }

        private void SendBytes(byte[] bytes)
        {
            if (bytes.Length <= 4)
            {
                return;
            }
            this.socket.Send(bytes);
        }

        private void Listen()
        {
            while (this.Run) {
                int len = this.ReceiveFullMessage();
                if (len < 0)
                {
                    if (len == -3)
                    {
                        this.Window.AddMessage("server quit\n");
                        this.Run = false;
                        break;
                    }
                    continue;
                }
                String message = Encoding.ASCII.GetString(this.buffer, 4, len) + "\n";
                this.Window.AddMessage(message);
            }
        }

        public Backend(LoginWindow Window)
        {
            this.Window = Window;
        }

        public void init()
        {
            if (!File.Exists(Backend.PubKeyFile))
            {
                throw new Exception("RSA Keypair file does not exist, try running the server first");
            }

            Console.WriteLine("Loading key pair\n");
            //read file
            using (StreamReader sr = new StreamReader(Backend.PubKeyFile))
            {
                string keytxt = sr.ReadToEnd();

                //load key from file
                this.CSP_Parameters.KeyContainerName = Backend.KeyName;
                this.rsa = new RSACryptoServiceProvider(this.CSP_Parameters);
                this.rsa.PersistKeyInCsp = true;
                this.rsa.FromXmlString(keytxt);
            }
            try
            {
                //IP information
                int port = 11000;
                IPAddress ipAddress = Dns.GetHostAddresses("localhost")[0];
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                // Connect the socket
                this.socket = new Socket(AddressFamily.InterNetworkV6, SocketType.Stream, ProtocolType.Tcp);
                this.socket.Connect(remoteEP);

                this.socket.ReceiveTimeout = 1000 * 60 * 10;

                this.Listener = new Thread(this.Listen);
                this.Listener.Start();
            }
            catch (Exception)
            {
                this.Window.Close();
            }
        }

        public void LogIn(String Username, String Password, bool save_autologinfo)
        {
            this.Username = Username;

            //username
            byte[] username_bytes = Encoding.ASCII.GetBytes(Username);
            byte[] encrypted_username = this.rsa.Encrypt(username_bytes, false);
            byte[] username_len_bytes = BitConverter.GetBytes(encrypted_username.Length); //always length 4
            byte[] full_username = new byte[encrypted_username.Length + 4];

            for (int i = 0; i < 4; ++i)
            {
                full_username[i] = username_len_bytes[i];
            }
            for (int i = 0, j = 4; i < encrypted_username.Length; ++i, ++j)
            {
                full_username[j] = encrypted_username[i];
            }

            //possibly send password, or auto log if there wasn't one given
            if (File.Exists(@".\autologpass") && Password == "")
            {
                byte[] autologinfo = ReadByteArray(@".\autologpass");
                this.SendBytes(autologinfo);
                this.SendBytes(full_username);
            }
            else
            {
                //password
                byte[] password_bytes = Encoding.ASCII.GetBytes(Password);
                byte[] encrypted_password = this.rsa.Encrypt(password_bytes, false);
                byte[] password_len_bytes = BitConverter.GetBytes(encrypted_password.Length); //always length 4

                //password
                byte[] full_password = new byte[encrypted_password.Length + 4];
                for (int i = 0; i < 4; ++i)
                {
                    full_password[i] = password_len_bytes[i];
                }
                for (int i = 0, j = 4; i < encrypted_password.Length; ++i, ++j)
                {
                    full_password[j] = encrypted_password[i];
                }

                //possibly save the info
                if (save_autologinfo)
                {
                    WriteByteArray(@".\autologpass", full_password);
                }

                //send
                this.SendBytes(full_password);
                this.SendBytes(full_username);
            }
        }

        public void Close()
        {
            this.Run = false;
            this.socket.Disconnect(true);
            this.socket.Close();
            this.Listener.Join();
        }

        public void SendMessage(string message)
        {
            if (message == "")
            {
                return;
            }
            //we're not checking endianness, because this should all be on the same machine
            byte[] message_bytes = Encoding.ASCII.GetBytes(message);
            byte[] len_bytes = BitConverter.GetBytes(message_bytes.Length); //always length 4

            byte[] full_message = new byte[message_bytes.Length + 4];

            for (int i = 0; i < 4; ++i)
            {
                full_message[i] = len_bytes[i];
            }
            for (int i = 0, j = 4; i < message_bytes.Length; ++i, ++j)
            {
                full_message[j] = message_bytes[i];
            }

            this.SendBytes(full_message);
        }
    }
}
