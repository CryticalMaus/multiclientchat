﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.InteropServices;


namespace Server
{
    class Program
    {
        //multi threaded client support
        private static Object ClientConnectionLock = new Object();
        private static LinkedList<Connection> ClientConnections = new LinkedList<Connection>();
        private static List<Thread> DeadConnections = new List<Thread>();

        //encryption
        private static CspParameters CSP_Parameters = new CspParameters();
        private static RSACryptoServiceProvider rsa;
        static string PubKeyFile = @".\rsaPublicKey.txt";
        static string KeyName = "KeyPair";

        //log messages to file
        [DllImport(@".\MessageLogSystem.dll")]//we also might want dynamic invoke instead
        private static extern void LogMessage([MarshalAs(UnmanagedType.LPStr)] string message);

        class Connection
        {
            byte[] bytes = new Byte[1024];

            //socket used to listen to client
            Socket handler = null;
            Thread worker;
            string ClientUsername = null;
            bool ClientIsValid = false;

            public Connection(Socket handler)
            {
                this.handler = handler;
                this.handler.ReceiveTimeout = 1000 * 60 * 10;
                this.worker = new Thread(this.run);
                this.worker.Start();
            }

            private bool CheckLoginInformation()
            {
                Program.WriteMessageLog("Checking possible client login information\n");

                //first receive the login information
                try
                {
                    //get password
                    byte[] encrypted_password = new byte[256];
                    int len = Program.ReceiveFullMessage(this.handler, ref encrypted_password);
                    if (len <= 0)
                    {
                        return false;
                    }

                    Program.WriteMessageLog("Received password\n");

                    //copy password to correct size
                    byte[] trimmed_encrypted_password = new byte[len];
                    for (int i = 0, j = 4; i < len; ++i, ++j)
                    {
                        trimmed_encrypted_password[i] = encrypted_password[j];
                    }

                    //decrypt password
                    byte[] decrypted_password = Program.rsa.Decrypt(trimmed_encrypted_password, false);
                    string password = Encoding.ASCII.GetString(decrypted_password, 0, decrypted_password.Length);

                    //for the sake of not creating encrypted user profiles, everyone has the same password
                    if (password != "ops tech")
                    {
                        Program.WriteMessageLog("Client gave bad password: " + password + "\n");
                        return false;
                    }

                    Program.WriteMessageLog("Accepted password\n");

                    //get username
                    byte[] encrypted_username = new byte[256];
                    len = Program.ReceiveFullMessage(this.handler, ref encrypted_username);
                    if (len <= 0)
                    {
                        return false;
                    }

                    Program.WriteMessageLog("Received username\n");

                    //copy username to correct size
                    byte[] trimmed_encrypted_username = new byte[len];
                    for (int i = 0, j = 4; i < len; ++i, ++j)
                    {
                        trimmed_encrypted_username[i] = encrypted_username[j];
                    }

                    //decrypt username
                    byte[] decrypted_username = Program.rsa.Decrypt(trimmed_encrypted_username, false);
                    this.ClientUsername = Encoding.ASCII.GetString(decrypted_username, 0, decrypted_username.Length);

                    Program.WriteMessageLog("Accepting client: "+ this.ClientUsername + "\n");
                    return true;
                }
                catch (TimeoutException)
                {
                    return false;
                }
            }

            private void run()
            {
                if (!this.CheckLoginInformation())
                {
                    Program.WriteMessageLog("Rejecting client\n");
                    this.quit();
                    return;
                }

                lock (this)
                {
                    this.ClientIsValid = true;
                }

                Program.WriteMessageLog("Validated user: " + this.ClientUsername + "\n");

                this.SendMessage(Program.EncodeMessage("Server: Login Accepted"));

                while (true)
                {
                    int len = Program.ReceiveFullMessage(this.handler, ref this.bytes);
                    if (len <= 0)
                    {
                        Program.WriteMessageLog(this.ClientUsername + " - gave bad message\n");
                        break;
                    }
                    string named_message = this.ClientUsername + ": " + Program.DecodeMessage(this.bytes, 4, len);

                    Program.WriteMessageLog(named_message + "\n");
                    lock (Program.ClientConnectionLock)
                    {
                        Program.BroadcastMessage(named_message);
                    }
                }
                this.quit();
            }

            private void quit()
            {
                //throwing an exception will exit the lock scope
                lock (Program.ClientConnectionLock)
                {
                    Program.ClientConnections.Remove(this);
                    Program.DeadConnections.Add(this.worker);
                    Program.WriteMessageLog("Connection Quitting\n");
                    handler.Disconnect(true);
                    this.handler.Close();
                }
            }

            public void SendMessage(byte[] Message)
            {
                lock (this)
                {
                    if (this.ClientIsValid) {
                        this.handler.Send(Message);
                    }
                }
            }
        }

        private static int ReceiveFullMessage(Socket ear, ref byte[] buffer)
        {
            int byte_count = 0;
            int message_length = -1;
            while (true)
            {
                try
                {
                    byte_count += ear.Receive(buffer);

                    //the first 4 bytes are the message length
                    if (byte_count < 4)
                    {
                        continue;
                    }
                    if (byte_count > buffer.Length)
                    {
                        return -1;
                    }
                    if (message_length < 0)
                    {
                        message_length = BitConverter.ToInt32(buffer, 0);
                    }
                    //wait for the whole message length
                    if (byte_count < message_length)
                    {
                        continue;
                    }
                    
                    return message_length;
                }
                catch (TimeoutException)
                {
                    return -2;
                }
                catch (System.Net.Sockets.SocketException)
                {
                    return -3;
                }
            }
        }

        private static byte[] EncodeMessage(string message)
        {
            //we're not checking endianness, because this should all be on the same machine
            byte[] message_bytes = Encoding.ASCII.GetBytes(message);
            byte[] len_bytes = BitConverter.GetBytes(message_bytes.Length); //always length 4

            byte[] full_message = new byte[message_bytes.Length + 4];

            for (int i = 0; i < 4; ++i)
            {
                full_message[i] = len_bytes[i];
            }
            for (int i = 0, j = 4; i < message_bytes.Length; ++i, ++j)
            {
                full_message[j] = message_bytes[i];
            }

            return full_message;
        }

        private static string DecodeMessage(byte[] message, int start, int length)
        {
            return Encoding.ASCII.GetString(message, start, length);
        }

        private static void BroadcastMessage(string message)
        {
            byte[] msg = EncodeMessage(message);

            foreach(Connection C in Program.ClientConnections)
            {
                C.SendMessage(msg);
            }
        }

        private static void WriteMessageLog(string message)
        {
            lock (Program.ClientConnectionLock)
            {
                Console.Write(message);
                Program.LogMessage(message);
            }
        }

        static void Main(string[] args)
        {
            //check if we've generated a keypair yet
            //note that both server and client will read from the same key file, which only occurs because this is a mock-up
            //in reality, the server would have the private key, and the client would have the public key
            if (File.Exists(Program.PubKeyFile))
            {
                Program.WriteMessageLog("Loading key pair\n");
                
                //read file
                using (StreamReader sr = new StreamReader(Program.PubKeyFile))
                {
                    string keytxt = sr.ReadToEnd();

                    //load key from file
                    Program.CSP_Parameters.KeyContainerName = Program.KeyName;
                    Program.rsa = new RSACryptoServiceProvider(Program.CSP_Parameters);
                    Program.rsa.PersistKeyInCsp = true;
                    Program.rsa.FromXmlString(keytxt);
                }

                if (Program.rsa.PublicOnly)
                {
                    throw new Exception("RSA key does not contain the private key for decryption");
                }
            }
            else
            {
                Program.WriteMessageLog("Generating new key pair...\n");
                //create key pair
                Program.CSP_Parameters.KeyContainerName = Program.KeyName;
                Program.rsa = new RSACryptoServiceProvider(Program.CSP_Parameters);
                Program.rsa.PersistKeyInCsp = true;

                //write key to file
                using (StreamWriter sw = new StreamWriter(Program.PubKeyFile, false))
                {
                    sw.Write(Program.rsa.ToXmlString(true));
                }
            }

            //IP information
            int port = 11000;
            IPAddress ipAddress = Dns.GetHostAddresses("localhost")[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);

            //begin listening with a TCP/IP socket
            try
            {
                Socket listener = new Socket(AddressFamily.InterNetworkV6, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(localEndPoint);
                listener.Listen(10);
                listener.ReceiveTimeout = 1000 * 60;// 1 minute

                // Start listening for connections.  
                while (true)
                {
                    //clean up dead connections
                    lock (Program.ClientConnectionLock)
                    {
                        while (DeadConnections.Count > 0)
                        {
                            DeadConnections.Last().Join();
                            DeadConnections.RemoveAt(DeadConnections.Count - 1);
                        }
                    }

                    Program.WriteMessageLog("Listening...\n");
                    //waits
                    try
                    {
                        Socket handler = listener.Accept();

                        //add a new client connection
                        lock (Program.ClientConnectionLock)
                        {
                            Program.WriteMessageLog("New possible client\n");
                            Program.ClientConnections.AddLast(new Connection(handler));
                        }
                    } catch(System.Net.Sockets.SocketException)
                    {
                        continue;
                    }
                }
            }
            catch (Exception e)
            {
                Program.WriteMessageLog(e.ToString());
            }

            Program.WriteMessageLog("\nServer shutting down\n");
        }
    }
}
